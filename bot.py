import os
import functools
import copy
import telebot
from telebot import types
from typing import List, Callable
import helper
import subprocess
import argparse

parser = argparse.ArgumentParser(description='Запуск Telegram Bot для Keenetic Bypass')
parser.add_argument('--prod', help='Установить режим production')
args = parser.parse_args()
IS_DEV = not args.prod

DEFAULT_ENV = os.getcwd() + '/default.env'
ENV_FILE = os.getcwd() + '/.env'

history = []
last_call = None
last_type = None
config = helper.reload_env(DEFAULT_ENV, ENV_FILE)
logger = helper.configure_logger(config)


def access_decorator(func):
    @functools.wraps(func)
    def wrapper(message, *args, **kwargs):
        user = message.from_user.username
        user_id = str(message.from_user.id)
        logger.debug(f'checking access for user {user if user else user_id}')
        access_users = helper.get_access_users(config)
        if user not in access_users and user_id not in access_users:
            answer = 'У вас нет доступа'
            logger.error(answer)
            return bot.send_message(message.chat.id, answer)
        return func(message, *args, **kwargs)
    return wrapper


def log_decorator(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        logger.info(f"Вызов функции '{func.__name__}'")
        return func(*args, **kwargs)
    return wrapper


def input_decorator(prev_call: Callable, text: str, custom_func: Callable = None):
    def decorator(func):
        def wrapper(message: types.Message, user_answer: str = None, *args, **kwargs):
            global last_call
            last_call = func if not isinstance(custom_func, Callable) else custom_func
            if not user_answer:
                print(prev_call)
                history.append(prev_call)
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
                markup.add(*generate_buttons('Назад'))
                return bot.send_message(message.chat.id, text, reply_markup=markup)
            return func(message, user_answer, *args, **kwargs)
        return wrapper
    return decorator


logger.info('Запуск приложения')
if not config.get('TELEGRAM_API'):
    logger.warning(f'В настройках файла {ENV_FILE} не указан параметр TELEGRAM_API')
    exit()
bot = None
try:
    bot = telebot.TeleBot(config.get('TELEGRAM_API'))
except Exception as e:
    logger.error(e)
    exit()


def generate_buttons(*args) -> List[types.KeyboardButton]:
    return [types.KeyboardButton(title) for title in args]


@bot.message_handler(commands=['start'])
@log_decorator
@access_decorator
def start(message):
    global last_type
    history.clear()
    last_type = None
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(*generate_buttons('Настройки', 'Списки обхода'))
    bot.send_message(message.chat.id, 'Добро пожаловать в меню!', reply_markup=markup)


@bot.message_handler(regexp='^Назад$')
@log_decorator
@access_decorator
def back(message):
    global last_call
    last_call = None
    func = start
    if history:
        func = history.pop()
    try:
        return func(message)
    except Exception as e:
        print(func)


@bot.message_handler(regexp='^Настройки$')
@log_decorator
@access_decorator
def menu_settings(message):
    history.append(start)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(*generate_buttons(*[
        'Установка и удаление',
        'Управление доступом',
        'Ключи',
        'Назад'
    ]))
    bot.send_message(message.chat.id, 'Настройки', reply_markup=markup)


@bot.message_handler(regexp='^Установка и удаление$')
@log_decorator
@access_decorator
def menu_setup(message):
    history.append(menu_settings)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(*generate_buttons(*[
        'Установка/Переустановка',
        'Удаление',
        'Назад'
    ]))
    bot.send_message(message.chat.id, 'Установка и удаление', reply_markup=markup)


@bot.message_handler(regexp='^Установка/Переустановка')
@log_decorator
@access_decorator
def menu_install(message):
    history.append(menu_setup)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    bot.send_message(message.chat.id, "Начинаем установку", reply_markup=markup)
    bot.send_message(message.chat.id, "Установка пакетов ...", reply_markup=markup)
    helper.install_packages(config, IS_DEV)
    bot.send_message(message.chat.id, "Установка пакетов завершена. Продолжаем установку", reply_markup=markup)
    bot.send_message(message.chat.id, "Создание файлов списков блокировки ...", reply_markup=markup)
    helper.install_unblock_files(config)
    bot.send_message(message.chat.id, "Созданы файлы списков блокировки", reply_markup=markup)
    bot.send_message(message.chat.id, "Установка изначальных скриптов ...", reply_markup=markup)
    helper.install_init_files(config, IS_DEV)
    bot.send_message(message.chat.id, "Установили изначальные скрипты", reply_markup=markup)
    bot.send_message(message.chat.id, "Скачавание основных скрипта разблокировок ...", reply_markup=markup)
    helper.install_base_scripts(config, IS_DEV)
    bot.send_message(message.chat.id, "Скачали 4 основных скрипта разблокировок", reply_markup=markup)
    bot.send_message(message.chat.id,
                     "Установка завершена. Теперь нужно немного донастроить роутер и перейти к"
                     "спискам для разблокировок. "
                     "Ключи для Vmess, Shadowsocks и Trojan необходимо установить вручную",
                     reply_markup=markup)
    start(message)


@bot.message_handler(regexp='^Удаление')
@log_decorator
@access_decorator
def menu_uninstall(message):
    history.append(menu_setup)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    bot.send_message(message.chat.id, 'Удаление ...', reply_markup=markup)
    helper.uninstall(config, IS_DEV)
    bot.send_message(message.chat.id, 'Успешно удалено', reply_markup=markup)
    start(message)


@bot.message_handler(regexp='^Управление доступом$')
@log_decorator
@access_decorator
def menu_access(message):
    history.append(menu_settings)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(*generate_buttons(*[
        'Список пользователей',
        'Добавить пользователя',
        'Удалить пользователя',
        'Назад'
    ]))
    bot.send_message(message.chat.id, 'Управление доступом', reply_markup=markup)


@bot.message_handler(regexp='^Список пользователей$')
@log_decorator
@access_decorator
def menu_access_list(message):
    history.append(menu_settings)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    answer = '\n'.join(map(lambda s: '- ' + s, helper.get_access_users(config)))
    bot.send_message(message.chat.id, answer, reply_markup=markup)


@bot.message_handler(regexp='^Добавить пользователя$')
@log_decorator
@access_decorator
@input_decorator(prev_call=menu_access, text='Введите список логинов через запятую для добавления')
def menu_access_add(message, user_answer: str = None):
    new_users = list(filter(bool, map(str.strip, (user_answer if user_answer else '').split(','))))
    access_users = helper.get_access_users(config)
    extra_users = set(new_users).union(set(access_users))
    helper.update_access_users(ENV_FILE, config, extra_users)
    answer = '\n'.join(map(lambda s: '- ' + s,
                           helper.get_access_users(config)))
    bot.send_message(message.chat.id, answer)
    return back(message)


@bot.message_handler(regexp='^Удалить пользователя$')
@log_decorator
@access_decorator
@input_decorator(prev_call=menu_access, text='Введите список логинов через запятую для удаления')
def menu_access_remove(message, user_answer: str = None):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(*generate_buttons('Назад'))
    access_users = helper.get_access_users(config)
    removed_users = list(filter(bool, map(str.strip, (user_answer if user_answer else '').split(','))))
    extra_users = set(access_users).difference(set(removed_users))
    helper.update_access_users(ENV_FILE, config, extra_users)
    answer = '\n'.join(map(lambda s: '- ' + s,
                           helper.get_access_users(config)))
    bot.send_message(message.chat.id, answer)
    return back(message)


@bot.message_handler(regexp='^Ключи')
@log_decorator
@access_decorator
def menu_keys(message):
    history.append(menu_settings)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(*generate_buttons(*[
        'Shadowsocks',
        'Vmess',
        'Trojan',
        'Назад'
    ]))
    bot.send_message(message.chat.id, 'Ключи', reply_markup=markup)


@bot.message_handler(regexp='^Shadowsocks$')
@log_decorator
@access_decorator
@input_decorator(prev_call=menu_keys, text='Скопируйте ключ сюда')
def menu_shadowsocks(message, user_answer: str = None):
    try:
        helper.save_shadowsocks(user_answer, config)
        etc_folder = config.get('ETC_FOLDER')
        call_file = etc_folder + '/init.d/S22shadowsocks'
        if os.path.exists(call_file) and not IS_DEV:
            subprocess.call([call_file, "restart"])
        bot.send_message(message.chat.id, 'Ключ успешно добавлен')
        back(message)
    except Exception as e:
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(*generate_buttons('Назад'))
        bot.send_message(message.chat.id, str(e), reply_markup=markup)


@bot.message_handler(regexp='^Vmess$')
@log_decorator
@access_decorator
@input_decorator(prev_call=menu_keys, text='Скопируйте ключ сюда')
def menu_vmess(message, user_answer: str = None):
    try:
        helper.save_vmess(user_answer, config)
        etc_folder = config.get('ETC_FOLDER')
        call_file = etc_folder + '/init.d/S24v2ray'
        if os.path.exists(call_file) and not IS_DEV:
            subprocess.call([call_file, "restart"])
        bot.send_message(message.chat.id, 'Ключ успешно добавлен')
        back(message)
    except Exception as e:
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(*generate_buttons('Назад'))
        bot.send_message(message.chat.id, str(e), reply_markup=markup)


@bot.message_handler(regexp='^Trojan$')
@log_decorator
@access_decorator
@input_decorator(prev_call=menu_keys, text='Скопируйте ключ сюда')
def menu_trojan(message, user_answer: str = None):
    try:
        helper.save_trojan(user_answer, config)
        etc_folder = config.get('ETC_FOLDER')
        call_file = etc_folder + '/init.d/S22trojan'
        if os.path.exists(call_file) and not IS_DEV:
            subprocess.call([call_file, "restart"])
        bot.send_message(message.chat.id, 'Ключ успешно добавлен')
        back(message)
    except Exception as e:
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(*generate_buttons('Назад'))
        bot.send_message(message.chat.id, str(e), reply_markup=markup)


@bot.message_handler(regexp='^Списки обхода')
@log_decorator
@access_decorator
def menu_unblock(message):
    global last_type
    last_type = None
    history.append(start)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(*generate_buttons(*[
        'Список Shadowsocks',
        'Список Vmess',
        'Список Trojan',
        'Назад'
    ]))
    bot.send_message(message.chat.id, 'Списки обхода', reply_markup=markup)


@bot.message_handler(regexp=r'^Список (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
def menu_unblock_action(message):
    global last_type
    history.append(menu_unblock)
    _type = helper.get_unblock_type(message.text)
    last_type = _type
    _type = _type.capitalize()
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(*generate_buttons(*[
        f'Показать список {_type}',
        f'Добавить в список {_type}',
        f'Добавить обход блокировок соцсетей для {_type}',
        f'Добавить обход блокировок ChatGPT для {_type}',
        f'Удалить из списка {_type}',
        f'Список бэкапов {_type}',
        f'Создать бэкап {_type}',
        f'Восстановить бэкап для {_type}',
        f'Удалить бэкап для {_type}',
        'Назад'
    ]))
    bot.send_message(message.chat.id, f'Список {_type}', reply_markup=markup)


@bot.message_handler(regexp=r'^Показать список (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
def menu_unblock_show(message):
    _type = helper.get_unblock_type(message.text)
    answer = f'Список {_type.capitalize()}:\n' + '\n'.join(helper.get_unblock_list(_type, config))
    bot.send_message(message.chat.id, answer)


@bot.message_handler(regexp=r'^Список бэкапов (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
def menu_unblock_backup_show(message):
    _type = helper.get_unblock_type(message.text)
    backups = helper.get_backups(_type, config)
    text = ''
    for fname, fdata in backups.items():
        text += f'{'-' * 10}\n{fname}:\n{'-' * 10}\n{fdata}\n'
    answer = f'Список бэкапов {_type.capitalize()}:\n' + text
    bot.send_message(message.chat.id, answer)


@bot.message_handler(regexp=r'^Добавить обход блокировок соцсетей для (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
def menu_unblock_add_social(message):
    _type = helper.get_unblock_type(message.text)
    helper.add_social_list(_type, config)
    answer = f'Список {_type.capitalize()}:\n' + '\n'.join(helper.get_unblock_list(_type, config))
    bot.send_message(message.chat.id, answer)


@bot.message_handler(regexp=r'^Добавить обход блокировок ChatGPT для (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
def menu_unblock_add_chatgpt(message):
    _type = helper.get_unblock_type(message.text)
    helper.add_chat_gpt(_type, config)
    answer = f'Список {_type.capitalize()}:\n' + '\n'.join(helper.get_unblock_list(_type, config))
    bot.send_message(message.chat.id, answer)


def modify_message(prefix: str, message: types.Message) -> types.Message:
    if not last_type or not isinstance(last_type, str):
        return message
    copy_message = copy.deepcopy(message)
    copy_message.text = prefix + ' ' + last_type.capitalize()
    return copy_message


def prev_unblock(prefix: str, func: Callable):
    def response(message, *args, **kwargs):
        if not last_type or not isinstance(last_type, str):
            return start(message)
        func(modify_message(prefix, message), *args, **kwargs)
    return response


def run_unblock_update():
    bin_folder = config.get('BIN_FOLDER')
    call_file = bin_folder + '/unblock_update.sh'
    if os.path.exists(call_file) and not IS_DEV:
        subprocess.call([call_file])


@bot.message_handler(regexp=r'^Добавить в список (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
@input_decorator(
    prev_call=prev_unblock('Список', menu_unblock_action),
    text='Введите список сайтов/ip адресов для добавления в список разблокировки',
)
def menu_unblock_add(message, user_answer: str = None):
    if last_type:
        helper.add_to_unblock(user_answer, last_type, config)
        answer = f'Список {last_type.capitalize()}:\n' + '\n'.join(helper.get_unblock_list(last_type, config))
        bot.send_message(message.chat.id, answer)
        run_unblock_update()
    menu_unblock_action(modify_message('Список', message))


@bot.message_handler(regexp=r'^Создать бэкап (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
def menu_unblock_add_backup(message):
    _type = helper.get_unblock_type(message.text)
    new_message = modify_message('Список ' + _type.capitalize(), message)
    try:
        helper.set_backup(_type, config)
        bot.send_message(message.chat.id, 'Бэкап успешно создан')
        run_unblock_update()
    except Exception as e:
        bot.send_message(message.chat.id, str(e))
    finally:
        return menu_unblock_show(new_message)


@bot.message_handler(regexp=r'^Восстановить бэкап для (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
@input_decorator(
    prev_call=prev_unblock('Список', menu_unblock_action),
    text='Введите название бэкапа',
)
def menu_unblock_restore_backup(message, user_answer: str = None):
    if last_type:
        helper.restore_backup(user_answer, last_type, config)
        answer = f'Список {last_type.capitalize()}:\n' + '\n'.join(helper.get_unblock_list(last_type, config))
        bot.send_message(message.chat.id, answer)
        run_unblock_update()
    menu_unblock_action(modify_message('Список', message))


@bot.message_handler(regexp=r'^Удалить бэкап для (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
@input_decorator(
    prev_call=prev_unblock('Список', menu_unblock_action),
    text='Введите название бэкапа',
)
def menu_unblock_remove_backup(message, user_answer: str = None):
    if last_type:
        helper.remove_backup(user_answer, last_type, config)
        answer = f'Список {last_type.capitalize()}:\n' + '\n'.join(helper.get_unblock_list(last_type, config))
        bot.send_message(message.chat.id, answer)
        run_unblock_update()
    menu_unblock_action(modify_message('Список', message))


@bot.message_handler(regexp=r'^Удалить из списка (Shadowsocks|Vmess|Trojan)$')
@log_decorator
@access_decorator
@input_decorator(
    prev_call=prev_unblock('Список', menu_unblock_action),
    text='Введите список сайтов/ip адресов для удаления из списка разблокировки',
)
def menu_unblock_add(message, user_answer: str = None):
    if last_type:
        helper.remove_from_unblock(user_answer, last_type, config)
        answer = f'Список {last_type.capitalize()}:\n' + '\n'.join(helper.get_unblock_list(last_type, config))
        run_unblock_update()
        bot.send_message(message.chat.id, answer)
    menu_unblock_action(modify_message('Список', message))


@bot.message_handler(content_types=['text'])
@log_decorator
@access_decorator
def input_command(message):
    if last_call is None or not isinstance(last_call, Callable):
        return start(message)
    return last_call(message, str(message.text))


try:
    logger.info('Старт бота')
    bot.infinity_polling()
except Exception as e:
    logger.error(e)
