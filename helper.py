import glob
from typing import Dict, List, Set
import base64
import json
import re
import os
import stat
import subprocess
from dotenv import dotenv_values
import logging
import requests
import shutil
import datetime


def cfile(filename):
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    return filename


def configure_logger(config: Dict) -> logging.Logger | Exception:
    handlers = [
        logging.StreamHandler()
    ]
    if int(config.get('LOG_TO_FILE', 0)) == 1:
        handlers.insert(0, logging.FileHandler('app.log'))
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s: [%(levelname)s] %(message)s',
        handlers=handlers
    )
    return logging.getLogger(__name__)


def reload_env(*files) -> Dict:
    try:
        config = dict()
        for file in files:
            if os.path.exists(file) and os.path.isfile(file):
                config.update(dotenv_values(file))
        return config
    except Exception:
        return dict()


def save_shadowsocks(key: str, config: Dict) -> Exception | None:
    pattern = r"ss://(?P<encodedkey>[^@]+)@(?P<server>[^:]+):(?P<port>\d+)"
    match = re.search(pattern, key)
    if not match:
        raise Exception('Неверный формат ключа, попробуйте снова')
    encoded_key = match.group("encodedkey")
    server = match.group("server")
    port = match.group("port")
    decode = str(base64.b64decode(encoded_key))[2:].split(':')
    if len(decode) < 2:
        raise Exception('Неверный формат ключа, попробуйте снова')
    password, method, *_ = decode
    with open(cfile(config.get('SHADOWSOCKS_FILE')), 'w') as f:
        json.dump({
            'server': [server],
            'mode': 'tcp_and_udp',
            'server_port': int(port),
            'password': password,
            'timeout': 86400,
            'method': method,
            'local_address': '::',
            'local_port': int(config.get('SHADOWSOCKS_PORT', 0)),
            'fast_open': False,
            'ipv6_first': True
        }, f, indent=4)
    return None


def save_trojan(key: str, config: Dict) -> Exception | None:
    pattern = re.compile(
        r"trojan://(?P<password>[^@]+)@(?P<server>[^:]+):(?P<port>\d+).*"
    )
    match = pattern.search(key)
    if not match:
        raise Exception('Неверный формат ключа, попробуйте снова')
    with open(cfile(config.get('TROJAN_FILE')), 'w') as f:
        json.dump({
            'run_type': 'nat',
            'local_addr': '::',
            'local_port': int(config.get('TROJAN_PORT', 0)),
            'remote_addr': match.group('server'),
            'remote_port': int(match.group('port')),
            'password': [
                match.group('password')
            ],
            'ssl': {
                'verify': False,
                'verify_hostname': False
            }
        }, f, indent=4)
    return None


def save_vmess(key: str, config: Dict) -> Exception | None:
    pattern = r"vmess://(?P<encodedkey>.+)"
    match = re.search(pattern, key)
    if not match:
        raise Exception('Неверный формат ключа, попробуйте снова')
    encoded_key = match.group("encodedkey")
    s = base64.b64decode(encoded_key).decode('utf8')
    jsondata = json.loads(s)
    with open(cfile(config.get('VMESS_FILE')), 'w') as f:
        json.dump({
            "log": {
                "access": "",
                "error": "",
                "loglevel": "none"
            },
            "inbounds": [
                {
                    "port": int(config.get('VMESS_PORT', 0)),
                    "listen": "::",
                    "protocol": "dokodemo-door",
                    "settings": {
                        "network": "tcp",
                        "followRedirect": True
                    }
                }
            ],
            "outbounds": [
                {
                    "tag": "proxy",
                    "protocol": "vmess",
                    "settings": {
                        "vnext": [
                            {
                                "address": str(jsondata.get('add', '')),
                                "port": int(jsondata.get('port', 0)),
                                "users": [
                                    {
                                        "id": str(jsondata.get('id', '')),
                                        "alterId": str(jsondata.get('aid', '')),
                                        "email": "t@t.tt",
                                        "security": "auto"
                                    }
                                ]
                            }
                        ]
                    },
                    "streamSettings": {
                        "network": "ws",
                        "security": "tls",
                        "tlsSettings": {
                            "allowInsecure": True,
                            "serverName": str(jsondata.get('add', '')),
                        },
                        "wsSettings": {
                            "path": "/" + str(jsondata.get('ps', '')),
                            "headers": {
                                "Host": str(jsondata.get('host', ''))
                            }
                        },
                        "tls": "tls"
                    },
                    "mux": {
                        "enabled": False,
                        "concurrency": -1
                    }
                }
            ],
            "routing": {
                "domainStrategy": "IPIfNonMatch",
                "rules": [
                    {
                        "type": "field",
                        "port": "0-65535",
                        "outboundTag": "proxy",
                        "enabled": True
                    }
                ]
            }
        }, f, indent=4)
    return None


def get_backups(_type: str, config: Dict) -> Dict:
    result = {}
    folder = config.get('UNBLOCK_FOLDER')
    if not folder or not os.path.exists(folder) and not os.path.isdir(folder):
        return result
    search_pattern = os.path.join(folder, f'{_type}_*.bak')
    files = glob.glob(search_pattern)
    if not files:
        return result
    for file in files:
        file_name = os.path.basename(file)
        with open(file, 'r') as f:
            result[file_name] = f.read()
    return result


def set_backup(_type: str, config: Dict) -> Exception | None:
    folder = config.get('UNBLOCK_FOLDER')
    file = os.path.join(folder, _type + '.txt')
    if not os.path.isfile(file):
        raise Exception(f'{os.path.basename(file)} не существует')
    with open(file, 'r') as f:
        data = f.read()
    if not data:
        raise Exception(f'Текущий список пуст')

    file_name = os.path.splitext(os.path.basename(file))[0]
    file_name += '_' + datetime.datetime.now().strftime('%Y%m%d%H%M%S') + '.bak'
    new_file = os.path.join(folder, file_name)
    with open(new_file, 'w') as f:
        f.write(data)
    with open(file, 'w'):
        pass
    return None


def restore_backup(backup: str, _type: str, config: Dict) -> Exception | None:
    folder = config.get('UNBLOCK_FOLDER')
    backup_file = os.path.join(folder, backup)
    if not os.path.isfile(backup_file):
        return
    with open(backup_file, 'r') as f:
        data = f.read()
    data = set(filter(bool, data.split('\n')))
    if not data:
        return
    save_unblock(data, _type, config)
    remove_backup(backup, _type, config)


def remove_backup(backup: str, _type: str, config: Dict) -> None:
    folder = config.get('UNBLOCK_FOLDER')
    backup_file = os.path.join(folder, backup)
    if os.path.isfile(backup_file):
        try:
            os.remove(backup_file)
        except:
            pass


def get_admin_user(config: Dict) -> str:
    return config.get('ADMIN_USER', '')


def get_access_users(config: Dict) -> List:
    admin_user = get_admin_user(config)
    access_users = list(
        filter(
            bool,
            config.get('EXTERNAL_USER', '').split(',')
        )
    )
    if admin_user:
        access_users.append(admin_user)
    return access_users


def update_access_users(filename: str, config: Dict, access_users: List | Set) -> Dict:
    if not os.path.exists(filename) or not os.path.isfile(filename):
        return config
    updated_list = list(access_users)
    admin_user = get_admin_user(config)
    if admin_user in updated_list:
        updated_list.remove(admin_user)
    config['EXTERNAL_USER'] = ','.join(updated_list)
    lines = []
    for key, value in config.items():
        lines.append(f'{key}={value}')
    with open(filename, 'w') as file:
        file.write('\n'.join(lines))
    return reload_env(filename)


def get_unblock_type(text: str) -> str | None:
    pattern = r'^.*(?P<type>(Shadowsocks|Trojan|Vmess))$'
    match = re.search(pattern, text)
    if not match:
        return None
    return match.group("type").lower()


def get_unblock_list(_type: str, config: Dict) -> List:
    result = []
    folder = config.get('UNBLOCK_FOLDER')
    if not folder:
        return result
    filename = os.path.join(folder, _type + '.txt')
    if os.path.exists(filename) and os.path.isfile(filename):
        with open(filename, 'r') as file:
            for line in file:
                result.append(line.strip())
    return result


def is_domain(text: str) -> bool:
    url_pattern = re.compile(r'\b(?:http[s]?://)?([a-zA-Z0-9.-]+\.[a-zA-Z]{2,})(?:/[\S]*)?\b')
    return bool(url_pattern.match(text))


def is_ip4(text: str) -> bool:
    ipv4_pattern = re.compile(r'\b(?:\d{1,3}\.){3}\d{1,3}(?:/\d{1,2})?\b')
    return bool(ipv4_pattern.match(text))


def is_ip6(text: str) -> bool:
    ipv6_pattern = re.compile(
        r'\b(?:(?:[a-fA-F0-9]{1,4}:){7}[a-fA-F0-9]{1,4}|(?:[a-fA-F0-9]{1,4}:){1,7}:|[a-fA-F0-9]{1,4}::(?:[a-fA-F0-9]{1,4}:){0,5}[a-fA-F0-9]{1,4}|::(?:[a-fA-F0-9]{1,4}:){0,6}[a-fA-F0-9]{1,4})(?:/\d{1,3})?\b')
    return bool(ipv6_pattern.match(text))


def save_unblock(records: List | Set, _type: str, config: Dict):
    if config.get('UNBLOCK_FOLDER'):
        with open(cfile(os.path.join(config.get('UNBLOCK_FOLDER'), _type + '.txt')), 'w') as file:
            file.write('\n'.join(sorted(records,
                                        key=lambda s: (int(is_domain(s)), int(is_ip4(s)), int(is_ip6(s)), s.lower())
                                        )))


def filter_unblock(message: str | Set) -> Set:
    return set(filter(
        lambda m: m and str(m).strip() and (is_domain(m) or is_ip4(m) or is_ip6(m)),
        message.split('\n')
    ))


def add_to_unblock(message: str, _type: str, config: Dict):
    lines = filter_unblock(message)
    current = set(get_unblock_list(_type, config))
    result = lines.union(current)
    save_unblock(result, _type, config)


def remove_from_unblock(message: str, _type: str, config: Dict):
    lines = filter_unblock(message)
    current = set(get_unblock_list(_type, config))
    result = current.difference(lines)
    save_unblock(result, _type, config)


def add_social_list(_type: str, config: Dict):
    url = "https://gitlab.com/tim11/kinetic_bypass/-/raw/main/configs/lists/social.txt"
    s = requests.get(url).text
    add_to_unblock(s, _type, config)


def add_chat_gpt(_type: str, config: Dict):
    url = "https://gitlab.com/tim11/kinetic_bypass/-/raw/main/configs/lists/chatgpt.txt"
    s = requests.get(url).text
    add_to_unblock(s, _type, config)


def trip_lines(text: str) -> str:
    return '\n'.join(map(str.lstrip, text.split('\n')))


def install_packages(config: Dict, is_dev: bool = False):
    etc_folder = '/opt/etc'
    unblock_folder = ''
    if config.get('UNBLOCK_FOLDER'):
        unblock_folder = config.get('UNBLOCK_FOLDER')
    if config.get('ETC_FOLDER'):
        etc_folder = config['ETC_FOLDER']
    script = trip_lines(f'#!/bin/sh\n\
              opkg update\n\
              opkg install mc bind-dig cron dnsmasq-full ipset iptables obfs4 shadowsocks-libev-ss-redir shadowsocks-libev-config v2ray trojan\n\
              mkdir -p {unblock_folder}')
    install_sh = etc_folder + '/install.sh'
    with open(cfile(install_sh), 'w') as file:
        file.write(script)
    if is_dev:
        return
    os.chmod(install_sh, stat.S_IRWXU)
    subprocess.call([install_sh])
    os.remove(install_sh)


def install_unblock_files(config: Dict):
    unblock_folder = ''
    if config.get('UNBLOCK_FOLDER'):
        unblock_folder = config.get('UNBLOCK_FOLDER')
    for _type in ['shadowsocks', 'trojan', 'vmess']:
        with open(cfile(os.path.join(unblock_folder, _type + '.txt')), 'w'):
            pass


def install_init_files(config: Dict, is_dev: bool = False):
    etc_folder = ''
    bin_folder = ''
    if config.get('ETC_FOLDER'):
        etc_folder = config.get('ETC_FOLDER')
    if config.get('BIN_FOLDER'):
        bin_folder = config.get('BIN_FOLDER')
    with open(cfile(etc_folder + '/ndm/fs.d/100-ipset.sh'), 'w') as file:
        file.write(trip_lines('#!/bin/sh\n\
            [ "$1" != "start" ] && exit 0\n\
            ipset create unblocksh hash:net -exist\n\
            ipset create unblockvmess hash:net -exist\n\
            ipset create unblocktroj hash:net -exist\n\
            #script0\n\
            #script1\n\
            #script2\n\
            #script3\n\
            #script4\n\
            #script5\n\
            #script6\n\
            #script7\n\
            #script8\n\
            #script9\n\
            exit 0'))
    if not is_dev:
        os.chmod(etc_folder + '/ndm/fs.d/100-ipset.sh', stat.S_IRWXU)
    with open(cfile(bin_folder + '/unblock_update.sh'), 'w') as file:
        file.write(trip_lines(f'#!/bin/sh\n\
            ipset flush unblocksh\n\
            ipset flush unblockvmess\n\
            ipset flush unblocktroj\n\
            {bin_folder}/unblock_dnsmasq.sh\n\
            {etc_folder}/init.d/S56dnsmasq restart\n\
            {bin_folder}/unblock_ipset.sh &'))
    if not is_dev:
        os.chmod(bin_folder + '/unblock_update.sh', stat.S_IRWXU)
    with open(cfile(etc_folder + '/init.d/S99unblock'), 'w') as file:
        file.write(trip_lines(f'#!/bin/sh\n\
            [ "$1" != "start" ] && exit 0\n\
            {bin_folder}/unblock_ipset.sh\n' +
            f'python {os.getcwd()}/bot.py --prod &'))
    if not is_dev:
        os.chmod(etc_folder + '/init.d/S99unblock', stat.S_IRWXU)
    lines = []
    crontab = cfile(etc_folder + '/crontab')
    if os.path.exists(crontab) and os.path.isfile(crontab):
        with open(crontab, 'r') as file:
            lines = file.readlines()
    new_line = f'00 06 * * * root {bin_folder}/unblock_ipset.sh'
    with open(crontab, 'w') as file:
        if not any(map(lambda s: s.strip() == new_line, lines)):
            file.write(new_line + '\n')
    if not is_dev:
        subprocess.call([bin_folder + "/unblock_update.sh"])


def install_base_scripts(config: Dict, is_dev: bool = False):
    etc_folder = bin_folder = unblock_folder = ''
    if config.get('ETC_FOLDER'):
        etc_folder = config.get('ETC_FOLDER')
    if config.get('BIN_FOLDER'):
        bin_folder = config.get('BIN_FOLDER')
    if config.get('UNBLOCK_FOLDER'):
        unblock_folder = config.get('UNBLOCK_FOLDER')
    url = "https://gitlab.com/tim11/kinetic_bypass/-/raw/main/configs/unblock_ipset.sh"
    s = requests.get(url).text
    s = s.replace("40500", config.get('DNS_OVER_TLS_PORT'))
    s = s.replace("/opt/etc/unblock", unblock_folder)
    with open(cfile(bin_folder + '/unblock_ipset.sh'), 'w') as file:
        file.write(s)
    if not is_dev:
        os.chmod(bin_folder + '/unblock_ipset.sh', stat.S_IRWXU)
    url = "https://gitlab.com/tim11/kinetic_bypass/-/raw/main/configs/unblock.dnsmasq"
    s = requests.get(url).text
    s = s.replace("40500", config.get('DNS_OVER_TLS_PORT'))
    s = s.replace("/opt/etc/unblock", unblock_folder)
    s = s.replace("/opt/etc", etc_folder)
    with open(cfile(bin_folder + '/unblock_dnsmasq.sh'), 'w') as file:
        file.write(s)
    if not is_dev:
        os.chmod(bin_folder + '/unblock_dnsmasq.sh', stat.S_IRWXU)
    url = "https://gitlab.com/tim11/kinetic_bypass/-/raw/main/configs/100-redirect.sh"
    s = requests.get(url).text
    s = s.replace("1082", config.get('LOCAL_PORT_SH'))
    s = s.replace("10810", config.get('VMESS_PORT'))
    s = s.replace("10829", config.get('TROJAN_PORT'))
    s = s.replace("192.168.1.1", config.get('ROUTER_IP'))
    with open(cfile(etc_folder + '/ndm/netfilter.d/100-redirect.sh'), 'w') as file:
        file.write(s)
    if not is_dev:
        os.chmod(etc_folder + '/ndm/netfilter.d/100-redirect.sh', stat.S_IRWXU)
    url = "https://gitlab.com/tim11/kinetic_bypass/-/raw/main/configs/dnsmasq.conf"
    s = requests.get(url).text
    s = s.replace("40500", config.get('DNS_OVER_TLS_PORT'))
    s = s.replace("40508", config.get('DNS_OVER_HTTPS_PORT'))
    s = s.replace("192.168.1.1", config.get('ROUTER_IP'))
    with open(cfile(etc_folder + '/dnsmasq.conf'), 'w') as file:
        file.write(s)
    if not is_dev:
        os.chmod(etc_folder + '/dnsmasq.conf', stat.S_IRWXU)


def uninstall(config: Dict, is_dev: bool = True):
    etc_folder = ''
    bin_folder = ''
    unblock_folder = ''
    if config.get('ETC_FOLDER'):
        etc_folder = config.get('ETC_FOLDER')
    if config.get('BIN_FOLDER'):
        bin_folder = config.get('BIN_FOLDER')
    if config.get('UNBLOCK_FOLDER'):
        unblock_folder = config.get('UNBLOCK_FOLDER')
    for file in [
        etc_folder + '/ndm/fs.d/100-ipset.sh',
        bin_folder + '/unblock_update.sh',
        etc_folder + '/init.d/S99unblock',
        bin_folder + '/unblock_ipset.sh',
        etc_folder + '/ndm/netfilter.d/100-redirect.sh',
        bin_folder + '/unblock_dnsmasq.sh'
    ]:
        if os.path.isfile(file):
            os.remove(file)
    shutil.rmtree(unblock_folder)
    crontab = cfile(etc_folder + '/crontab')
    lines = []
    line = f'00 06 * * * root {bin_folder}/unblock_ipset.sh'
    with open(crontab, 'r') as file:
        lines = file.readlines()
    lines = list(
        filter(lambda s: s.strip() != line, lines)
    )
    with open(cfile(crontab), 'w') as file:
        file.write('\n'.join(lines))
    if is_dev:
        return
    script = trip_lines(
        '#!/bin/sh\n\
        opkg update \n\
        opkg remove  mc tor tor-geoip bind-dig cron dnsmasq-full ipset iptables obfs4 shadowsocks-libev-ss-redir shadowsocks-libev-config')
    with open(cfile(etc_folder + '/remove.sh'), 'w') as file:
        file.write(script)
    os.chmod(cfile(etc_folder + '/remove.sh'), stat.S_IRWXU)
    subprocess.call([cfile(etc_folder + '/remove.sh')])
    os.remove(cfile(etc_folder + '/remove.sh'))
