#!/bin/bash
if command -v curl > /dev/null; then
  curl -Lo keenetic_bypass.zip https://gitlab.com/tim11/kinetic_bypass/-/jobs/7038529494/artifacts/download?file_type=archive
else
  wget -O keenetic_bypass.zip https://gitlab.com/tim11/kinetic_bypass/-/jobs/7038529494/artifacts/download?file_type=archive
fi
unzip -o keenetic_bypass.zip -d bot_keenetic_bypass
cd bot_keenetic_bypass
unzip -o keenetic_bypass.zip
pip install -r requirements.txt
rm install.sh
rm timestamp.txt
rm requirements.txt
rm keenetic_bypass.zip
cd ..
rm keenetic_bypass.zip
